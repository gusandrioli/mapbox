
mapboxgl.accessToken = 'pk.eyJ1IjoiZmVybmFuZG9vcnRlZ2EiLCJhIjoiY2p4bmh3aWp3MGVzZzNobzZiaXJzOWplMiJ9.btqXzpFrFAHjtG9IRRlxwg';
var map = new mapboxgl.Map({
    style: 'mapbox://styles/fernandoortega/cjyd8nkci0vot1ckyi479ajef',
    center: [-74.0066, 40.7135],
    zoom: 15.5,
    pitch: 45,
    bearing: -17.6,
    container: 'map',
    antialias: true
});